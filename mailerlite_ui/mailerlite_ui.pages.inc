<?php

/**
 * Title callback: Returns the name of the group.
 */
function mailerlite_ui_view_page_title($group) {
  return $group->name;
}

/**
 * Page callback wrapper for mailerlite view.
 *
 * @param int $group_id
 *   Group id.
 */
function mailerlite_ui_view_group_form($form, &$form_state, $group) {
  
  $header = array(
    'email' => t('Email'),  
  );
  
  $options = array();
  $options = mailerlite_get_group_subscribers($group->id);

  foreach ($options as $option) {
    $options_arr[] = array(l($option['email'], 'admin/people/mailerlite-subscriber/' . $option['email']), l(t('edit'), 'admin/people/mailerlite-subscriber/' . $option['email'] . '/edit'));
  }

  return [
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $options_arr,
  ];
}

/**
 * Form constructor for the mailer lite create form.
 *
 * @see mailerlite_ui_group_form_submit()
 */
function mailerlite_ui_group_form($form, $form_state, $group = NULL) {

  $roles = user_roles();
  $default_roles = array();
  $registration_groups = variable_get('mailerlite_user_registration_groups', array());
  
  $form['name'] = [
    '#type' => 'textfield',
    '#default_value' => isset($group) ? $group->name : '',
    '#title' => t('Group Name'),
  ];
  
  if (isset($group)) {
    $form['id'] = [
      '#type' => 'value',
      '#value' => $group->id,
    ];
    $form['actions']['delete'] = array(
      '#type' => 'markup',
      '#markup' => l(t('Delete'), 'admin/structure/mailerlite/group/' . $group->id . '/delete'),
      '#weight' => 9,
    );
    $default_roles = variable_get('mailerlite_group_access_' . $group->id, array());
  }

  $form['registration'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show on user registion form.'),
    '#default_value' => isset($registration_groups[$group->id]) ? TRUE : FALSE,
    '#description' => t('If this option is selected, "authenticated user" will be selected as well.'),
  );
  
  $form['roles'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Roles'),
    '#description' => t('Allow subscribtion to this group only for the selected role(s).'),
    '#options' => user_roles(),
    '#default_value' => $default_roles,
  );
  
  $form['actions'] = array(
    '#type' => 'actions',
  );
  $form['actions']['submit'] = [
    '#type' => 'submit',
    '#value' => t('Save'),
  ];

  return $form;
}

/**
 * Form submission handler for mailerlite_ui_group_form().
 */
function mailerlite_ui_group_form_submit($form, $form_state) {
  
  $group_name = array(
    'name' => $form_state['values']['name'],
  );
  $groups = mailerlite_client_groups();

  if (!isset($form_state['values']['id'])) {
    $group = $groups->create($group_name);
  }
  else {
    $group = $groups->update($form_state['values']['id'], $group_name);
  }
  
  $registration_groups = variable_get('mailerlite_user_registration_groups', array());
  $registration_groups[$group->id] = $form_state['values']['registration'];
  variable_set('mailerlite_user_registration_groups',array_filter($registration_groups));
  
  if ($form_state['values']['registration']) {
    $form_state['values']['roles'][2] = TRUE;
  }
  
  $roles = array_filter($form_state['values']['roles']);
  $access = array_keys($roles);
  variable_set('mailerlite_group_access_' . $group->id, $access);
}


/**
 * Form constructor for the mailer lite deletion confirmation form.
 *
 * @see mailerlite_ui_delete_confirm_submit()
 */
function mailerlite_ui_delete_confirm($form, &$form_state, $group) {

  // Always provide entity id in the same form key as in the entity edit form.
  $form['id'] = array(
    '#type' => 'value', 
    '#value' => $group->id
  );
  return confirm_form($form, 
    t('Are you sure you want to delete %mailerlite_group_name?', array('%mailerlite_group_name' => $group->name)), 'admin/structure/mailerlite/group/' . $group->id . '/edit', 
    t('This action cannot be undone.'), 
    t('Delete'), 
    t('Cancel')
  );
}

/**
 * Executes mailer lite deletion.
 *
 * @see mailerlite_ui_delete_confirm()
 */
function mailerlite_ui_delete_confirm_submit($form, &$form_state) {
  if ($form_state['values']['confirm']) {
    $id = $form_state['values']['id'];

    mailerlite_ui_delete($id);
  }
  $form_state['redirect'] = 'admin/structure/mailerlite/list';
}

/**
 * Deletes a mailer lite.
 *
 * @param int $id
 *   A mailer lite ID.
 */
function mailerlite_ui_delete($id) {
  $groups = mailerlite_client_groups();

  if (!isset($form_state['values']['id'])) {
    $groups->delete($id);
  }
}
